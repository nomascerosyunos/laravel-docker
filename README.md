# Docker compose file for laravel development

## Starting docker

`docker-compose up -d`

### Running artisan

`docker-compose exec app php artisan`

### Running composer (eg: composer install)

`docker run -rm -v ${PWD}:/app composer/composer install`